# Project Euler
Solutions to [Project Euler](https://projecteuler.net) problems in [Python](https://python.org)

## Requirements
To run a solver:
  + Python 3

To use the `run.sh` script:
  + Bash

## Usage
Using `run.sh` (Recommended):

```console
$ ./run.sh PROBLEM_NUMBER
```

Running a solver directly:

```console
$ cd solvers/PROBLEM_NUMBER
$ ./main.py
```

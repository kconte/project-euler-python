#!/bin/sh

[ -z $1 ] && echo "no problem provided" && exit 1
BRANCH="p$1"

git checkout main
git merge $BRANCH
git branch -D $BRANCH

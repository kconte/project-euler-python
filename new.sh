#!/bin/bash

set -euo pipefail

[ -z $1 ] && echo "no problem provided" && exit 1

BRANCH="p$1"
SOLVER_PATH="./solvers/$(printf "%03d" $1)"

git checkout main
git checkout -b $BRANCH
cp -r ./solvers/template $SOLVER_PATH
sed -i "3 s/$/$1/" $SOLVER_PATH/main.py
code "$SOLVER_PATH/main.py"

#!/bin/bash

set -euo pipefail

SOLVER_ROOT="./solvers"
[ -z $1 ] && echo "no solver provided" && exit 1

SOLVER_PATH="./solvers/$(printf "%03d" $1)"
cd $SOLVER_PATH
python3 main.py
cd - >/dev/null

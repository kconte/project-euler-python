#!/usr/bin/env python3

# https://projecteuler.net/problem=12

def main():
  # calculate the n-th Triangle number
  def T(n):
    return n * (n + 1) // 2

  # calculate the number of divisors of `n`
  def d(n):
    if n == 0:
      return 0

    from math import isqrt
    sqrt = isqrt(n)
    count = 0
    for fac in range(1, sqrt + 1):
      if n % fac == 0:
        count += 1
        if fac * fac != n:
          count += 1

    return count

  def solve():
    n = 1
    val = T(n)
    while d(val) < 500:
      n += 1
      val = T(n)
    return val

  print(solve())

if __name__ == "__main__":
  main()

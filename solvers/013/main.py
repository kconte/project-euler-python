#!/usr/bin/env python3

# https://projecteuler.net/problem=

def main():
  with open("./input.txt") as f:
    nums = list(map(int, f.read().strip().split("\n")))

  print(str(sum(nums))[:10])

if __name__ == "__main__":
  main()

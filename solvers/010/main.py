#!/usr/bin/env python3

# https://projecteuler.net/problem=10

from math import isqrt

def main():

  def sieve(n):
    N = n + 1
    is_prime = [True] * (N)
    is_prime[0] = is_prime[1] = False

    for i in range(2, isqrt(n) + 1):
      if is_prime[i]:
        for j in range(i * i, N, i):
          is_prime[j] = False

    return [i for i, v in enumerate(is_prime) if v]

  print(f"Example: {sum(sieve(10))}")
  print(f"Example: {sum(sieve(2_000_000))}")

if __name__ == "__main__":
  main()

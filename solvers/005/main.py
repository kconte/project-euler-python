#!/usr/bin/env python3

# https://projecteuler.net/problem=5

def main():

  def factorize(n):
    factors = list()
    factor = 2
    while n >= factor:
      count = 0
      while n % factor == 0:
        n //= factor
        count += 1
      if count > 0:
        factors.append((factor, count))
      factor += 1
    return factors

  def solve(n):
    all_factors = dict()
    for i in range(2, n + 1):
      factors = factorize(i)
      for (factor, count) in factors:
        all_factors[factor] = max(all_factors.get(factor, 0), count)

    res = 0
    for (base, exp) in all_factors.items():
      val = base ** exp
      if res > 0:
        res *= val
      else:
        res = val
    return res

  print(solve(20))

if __name__ == "__main__":
  main()

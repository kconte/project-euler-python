Consider the following sum of multiples of 3:

```
3 + 6 + 9 + 12 + ...
```

This can be rewritten as:
```
3 * (1 + 2 + 3 + 4 ...)
```

Sums of consecutive natural numbers beginning with `1` are called Triangle Numbers
and can be computed as such:

Define `T(n)` to be the `n-th` triangle number.

```
T(n) = (n * (n + 1)) / 2
```

So, using the above sum and the concept of Triangle numbers, we can write:

```
  3 + 6 + 9 + 12 + ...
= 3 * (1 + 2 + 3 + 4 ...)
= 3 * T(n)
```

Now all that's left is to calculate `n`.
For sums of three, take the upper limit of the problem (in this case `999`).
Using integer division, take `n = 999 \ 3 = 333`

The same logic applies for multiples of `5`.

The last step in this algorithm is to remove numbers that have been "double-counted".
That is, numbers that are multiples of both `3` and `5`.
These numbers show up in both sums, and skew the results if considered more than once.

To account for this, simply subtract multiples of `15`. Performing this subtraction once
will remove any duplicate considerations.

We are left with a concise solution:

```
3-mult  = 333
5-mult  = 199
15-mult = 66

ANSWER: 3 * T(3-mult) + 5 * T(5-mult) - 15 * T(15-mult)
```

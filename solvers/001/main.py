#!/usr/bin/env python3

# https://projecteuler.net/problem=1

def main():

  def T(n):
    return n * (n + 1) // 2

  n = 999 // 3
  ans = 3 * T(n)

  n = 999 // 5
  ans += 5 * T(n)

  n = 999 // 15
  ans -= 15 * T(n)

  print(ans)

if __name__ == "__main__":
  main()

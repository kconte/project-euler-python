#!/usr/bin/env python3

# https://projecteuler.net/problem=6

def main():
  sum_square = lambda n: sum(i * i for i in range(n + 1))
  square_sum = lambda n: sum(i for i in range(n + 1)) * sum(i for i in range(n + 1))

  print(f"Example: {square_sum(10) - sum_square(10)}")
  print(f"Solution: {square_sum(100) - sum_square(100)}")

if __name__ == "__main__":
  main()

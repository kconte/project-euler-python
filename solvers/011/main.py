#!/usr/bin/env python3

# https://projecteuler.net/problem=11

from functools import update_wrapper


def main():
  # Reads and parses a ' '-separated columns, '\n'-separated rows grid
  # into a list(list(int)) and returns the result
  def read_grid_from_file(filepath):
    with open(filepath) as f:
      rows = f.read().strip().split("\n")
    grid = []
    for row in rows:
      row = row.split(" ")
      row = list(map(int, row))
      grid.append(row)
    return grid

  # Transposes a matrix
  def transpose(m):
    N = len(m)
    T = [ [ 0 for _ in range(N) ] for _ in range(N) ]

    for i in range(N):
      for j in range(N):
        T[i][j] = m[j][i]

    return T

  # Returns overlapping windows of a one-dimensional list
  def windows(xs, l):
    assert len(xs) >= l
    start = 0
    end = l
    while end <= len(xs):
      yield xs[start:end]
      start += 1
      end += 1

  # similar to the built-in sum function
  def prod(xs):
    from functools import reduce
    return reduce(lambda x,y: x * y, xs)

  grid = read_grid_from_file("./grid.txt")
  grid_t = transpose(grid)

  ans = 0

  # left/right
  for i in range(len(grid)):
    for window in windows(grid[i], 4):
      ans = max(ans, prod(window))

  # up/down
  for j in range(len(grid_t)):
    for window in windows(grid_t[j], 4):
      ans = max(ans, prod(window))

  # diagonally down
  for i in range(len(grid) - 4):
    for j in range(len(grid) - 4):
      window = [
        grid[i][j],
        grid[i + 1][j + 1],
        grid[i + 2][j + 2],
        grid[i + 3][j + 3],
      ]
      ans = max(ans, prod(window))

  # diagonally up
  for i in range(3, len(grid)):
    for j in range(len(grid) - 4):
      window = [
        grid[i][j],
        grid[i - 1][j + 1],
        grid[i - 2][j + 2],
        grid[i - 3][j + 3],
      ]
      ans = max(ans, prod(window))


  print(ans)

if __name__ == "__main__":
  main()

#!/usr/bin/env python3

# https://projecteuler.net/problem=17

def main():

  DIGITS = [ "zero", "one", "two", "three", "four", "five", "six",
                       "seven", "eight", "nine", "ten", "eleven", "twelve",
                       "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
                       "eighteen", "nineteen" ]

  TENS = [ None, None, "twenty", "thirty", "forty", "fifty", "sixty", "seventy",
           "eighty", "ninety" ]

  def int_to_words(n: int) -> str:
    if n > 1_000:
      raise Exception(f"[ERROR]: `{n}` outside range of [0, 1000]")

    elif n == 1_000:
      return "one thousand"

    elif n >= 100:
      digit = n // 100
      rest = n % 100
      if rest > 0:
        return DIGITS[digit] + " hundred and " + int_to_words(rest)
      else:
        return DIGITS[digit] + " hundred"

    elif n >= 20:
      digit = n // 10
      rest = n % 10
      if rest > 0:
        return TENS[digit] + "-" + int_to_words(rest)
      else:
        return TENS[digit]

    elif n >= 0:
      return DIGITS[n]


  count = 0
  for i in range(1, 1001):
    words = int_to_words(i)
    count += len(words.replace(" ", "").replace("-", ""))

  print(count)

if __name__ == "__main__":
  main()

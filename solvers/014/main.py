#!/usr/bin/env python3

# https://projecteuler.net/problem=14

from functools import cache

def main():

  @cache
  def collatz(n):
    if n == 1:
      return 1
    elif n % 2 == 0:
      return 1 + collatz(n // 2)
    else:
      return 1 + collatz(3 * n + 1)

  max_collatz = 1
  ans = 0

  for n in range(1, 1_000_000):
    chain = collatz(n)
    if chain > max_collatz:
      max_collatz = chain
      ans = n

  print(ans)

if __name__ == "__main__":
  main()

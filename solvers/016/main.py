#!/usr/bin/env python3

# https://projecteuler.net/problem=16

def main():
  digits = list(map(int, list(str(2**1000))))

  ans = sum(digits)
  print(ans)

if __name__ == "__main__":
  main()

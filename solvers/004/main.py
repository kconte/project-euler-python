#!/usr/bin/env python3

# https://projecteuler.net/problem=4

def main():
  def reverse(n):
    assert n >= 0
    rev = 0
    while n:
      rev = (rev * 10) + (n % 10)
      n //= 10
    return rev

  def solve():
    palindromes = set()
    for a in range(999, 100, -1):
      for b in range(999, 100, -1):
        c = a * b
        if reverse(c) == c:
          palindromes.add(c)
    return max(palindromes)

  ans = solve()

  print(ans)

if __name__ == "__main__":
  main()

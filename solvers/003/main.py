#!/usr/bin/env python3

# https://projecteuler.net/problem=3

def main():
  def solve(n):
    i = 1
    while n > i:
      i += 1
      while n % i == 0:
        n /= i
    return max(n, i)

  print(f"Example: {solve(13_195)}")
  print(f"Solution: {solve(600_851_475_143)}")


if __name__ == "__main__":
  main()

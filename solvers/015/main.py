#!/usr/bin/env python3

# https://projecteuler.net/problem=15

def main():

  def factorial(n):
    from functools import reduce
    if n == 0: return 1
    return reduce(lambda x, y: x * y, range(1, n + 1))

  N = 20
  M = 20
  ans = factorial(M + N) // (factorial(M) * factorial(N))

  print(ans)

if __name__ == "__main__":
  main()

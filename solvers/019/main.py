#!/usr/bin/env python3

# https://projecteuler.net/problem=19

def main():
  def is_leap_year(yr: int):
    if yr % 4 != 0:
      return False

    if yr % 100 == 0:
      if yr % 400 == 0:
        return True
      return False

    return True

  def days_in_months(yr):
    return [
      31, 29 if is_leap_year(yr) else 28,
      31, 30, 31, 30, 31, 31, 30, 31, 30, 31,
    ]

  def tests():
    print("testing method `is_leap_year`")
    assert is_leap_year(1900) == False
    assert is_leap_year(2000) == True
    assert is_leap_year(2012) == True
    assert is_leap_year(1999) == False

    print("testing method `days_in_months`")
    assert \
      days_in_months(1900) == [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    assert \
      days_in_months(1904) == [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

    print("all tests passed")

  dow = 1

  # pre-compute the day of the week for 1 Jan 1901
  dim = days_in_months(1900)
  for month in range(12):
    dow = (dow + dim[month]) % 7

  ans = 0
  for yr in range(1901, 2001):
    dim = days_in_months(yr)
    for month in range(12):
      dow = (dow + dim[month]) % 7
      if dow == 0:
        ans += 1

  print(ans)

if __name__ == "__main__":
  main()

#!/usr/bin/env python3

# https://projecteuler.net/problem=22

def main():

  with open("./names.txt") as f:
    names = sorted(f.read().strip().replace("\"", "").split(","))

  ans = 0
  for i, name in enumerate(names, start = 1):
    score = 0
    for char in name:
      score += ord(char) - ord('A') + 1
    score *= i
    ans += score

  print(ans)

if __name__ == "__main__":
  main()

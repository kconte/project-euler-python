#!/usr/bin/env python3

# https://projecteuler.net/problem=21

def main():

  def d(n):
    from math import isqrt
    if n < 1:
      return 0

    s = 1
    for i in range(2, isqrt(n) + 1):
      if n % i == 0:
        s += i
        if i * i != n:
          s += n // i
    return s

  def tests():
    assert d(220) == 284
    assert d(284) == 220
    assert d(49) == 8

  tests()

  ans = 0
  for a in range(10_000):
    b = d(a)
    res = d(b)
    if res == a and a < b:
      ans += a + b

  print(ans)


if __name__ == "__main__":
  main()

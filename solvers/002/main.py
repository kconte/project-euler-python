#!/usr/bin/env python3

# https://projecteuler.net/problem=2

def main():

  LIMIT = 4_000_000

  def sol1():
    a = 0
    b = 1
    fib = a + b
    ans = 0

    while fib < LIMIT:
      a = b
      b = fib
      fib = a + b
      if fib % 2 == 0:
        ans += fib

    return ans

  def sol2():
    a = 1
    b = 1
    fib = a + b
    ans = 0

    while fib < LIMIT:
      ans += fib
      a = b + fib
      b = fib + a
      fib = a + b

    return ans

  def sol3():
    from functools import cache

    @cache
    def E(n):
      if n < 1: return 0
      elif n == 1: return 2
      else: return 4 * E(n - 1) + E(n - 2)

    ans = 0
    n = 0
    fib = E(n)
    while fib < LIMIT:
      ans += fib
      n += 1
      fib = E(n)

    return ans

  print(f"Solution 1: {sol1()}")
  print(f"Solution 2: {sol2()}")
  print(f"Solution 2: {sol3()}")

if __name__ == "__main__":
  main()

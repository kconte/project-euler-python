### `sol1`:
Simple brute-force solution.

### `sol2`:
Consider the Fibonacci Sequence:
```
0 1 1 2 3 5 8 13 21 34 ...
```
We can see that every two numbers there is an even number.
Thus, we can modify the brute-force solution to only sum the even
Fibonacci numbers (as the problem requests) without any modular arithmetic.

### `sol3`:
This is where things get interesting.

Let `F(n)` define the Fibonacci recurrence relation:
```
F(n) = F(n - 1) + F(n - 2)
```

Consider the sequence of even-valued Fibonacci terms:
```
0 2 8 34 144 ...
```
We can define a function `E(n)` to describe this recurrence relation:
```
E(n) = 4 * E(n - 1) + E(n - 2)
```
Which can be easily verified.

All that's left is to sum values of `E(n)` less than the provided limit of 4,000,000.

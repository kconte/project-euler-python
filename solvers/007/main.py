#!/usr/bin/env python3

# https://projecteuler.net/problem=7

from math import isqrt

def main():

  def is_prime(n):
    if n == 2: return True
    elif n < 2 or n % 2 == 0: return False
    for i in range(3, isqrt(n) + 1, 2):
      if n % i == 0:
        return False
    return True

  count = 1
  i = 1
  while count <= 10_000:
    i += 2
    if is_prime(i):
      count += 1

  print(i)

if __name__ == "__main__":
  main()

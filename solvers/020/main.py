#!/usr/bin/env python3

# https://projecteuler.net/problem=20

def main():
  def factorial(n):
    from functools import reduce
    if n == 0: return 1
    else: return reduce(lambda x, y: x * y, range(1, n + 1))

  ans = sum(map(int, list(str(factorial(100)))))
  print(ans)

if __name__ == "__main__":
  main()

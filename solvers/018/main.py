#!/usr/bin/env python3

# https://projecteuler.net/problem=18

from os import read


def main():

  def read_triangle_from_file(filepath):
    with open(filepath) as f:
      rows = f.read().strip().split("\n")

    triangle = []
    for row in rows:
      row = row.strip().split(" ")
      row = list(map(int, row))
      triangle.append(row)
    return triangle

  triangle = read_triangle_from_file("./triangle.txt")

  for i in range(len(triangle) - 2, -1, -1):
    for j in range(len(triangle[i])):
      triangle[i][j] += max(triangle[i + 1][j], triangle[i + 1][j + 1])

  ans = triangle[0][0]
  print(ans)

if __name__ == "__main__":
  main()

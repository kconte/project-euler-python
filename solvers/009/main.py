#!/usr/bin/env python3

# https://projecteuler.net/problem=9

def main():

  def solve():
    for a in range(1, 1000):
      for b in range(1, 1000):
        c = 1000 - a - b
        if a * a + b * b == c * c:
          return a * b * c

  print(solve())

if __name__ == "__main__":
  main()
